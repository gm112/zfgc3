<?php

@ini_set("output_buffering", "Off");
@ini_set('implicit_flush', 1);
@ini_set('zlib.output_compression', 0);
@set_time_limit(360000)

function niceUrl($val) {
	return preg_replace("/\\W/i", "_", strtolower($val));
}

$fnTime = 0;
function statusLog($msg) {
	global $fnTime;
	$fnTime = microtime(true);
	echo $msg."<br/>\n";
	flush();
	ob_flush();
}

function statusDone() {
	global $fnTime;
	$fTime = microtime(true) - $fnTime;
	echo "Done in ".$fTime." seconds<br/>\n";
	flush();
	ob_flush();
}

function mysqldate($timestamp) {
	return date('Y-m-d H:i:s', $timestamp);
}

function strlimit($string, $limit) {
	if(strlen($string) > $limit) {
		return substr($string, 0, $limit-3)."...";
	}
	return $string;
}

?><!DOCTYPE html>
<html>
	<body onload="finLoad();">
<?php
$Start = $_GET['start']?$_GET['start']:0;
$Count = 1000;

$convertPosts = true;
$convertTopics = false;
$convertBoards = false;

$DB_NAME = 'zfgc_smf2';
$DB_HOST = '127.0.0.1';
$DB_USER = 'root';
$DB_PASS = 'visionslive';

statusLog("Connecting...");
$handle = new mysqli($DB_HOST, $DB_USER, $DB_PASS, "zfgc_smf2");
$vhandle = new mysqli($DB_HOST, $DB_USER, $DB_PASS, "zfgc_vanilla");
statusDone();

if($convertBoards) {
	//ConvertBoards
	statusLog("Converting boards");
	$Query = "SELECT * FROM `smf_1categories`";
	$Result = $handle->query($Query) or die($handle->error.__LINE__);

	$vhandle->query("DELETE FROM gdn_category WHERE CategoryID != -1");
	$vhandle->query("UPDATE gdn_category SET TreeRight = 2, TreeLeft = 1 WHERE CategoryID = -1");
	$insert = $vhandle->prepare("INSERT INTO gdn_category (`CategoryID`, `ParentCategoryID`, `Depth`, `TreeLeft`, `TreeRight`, `Name`, `InsertUserID`, `UpdateUserID`, `DateInserted`, `DateUpdated`, `PermissionCategoryID`, `URLCode`) VALUES (?, -1, ?, ?, ?, ?, 1, 1, FROM_UNIXTIME(1140847200), FROM_UNIXTIME(1140847200), ?, ?)");
	//$inUserID; $inName; $inPassword; $inHashMethod; $inPhoto; $inAbout; $inEmail; $inShowEmail; $inGender; $inCountNotifications;
	$insert->bind_param("iiiisis", $inCategoryID, $inDepth, $inTreeLeft, $inTreeRight, $inName, $inPermissionCategoryID, $inURLCode); //i=integer

	if($Result->num_rows > 0) {
		while($row = $Result->fetch_assoc()) {
			$inCategoryID = $row['id_cat'];
			$inName = $row['name'];
			$inPermissionCategoryID = -1;
			$inURLCode = niceUrl($row['name']);
			$inDepth = 1;
			$inTreeLeft = 2;
			$inTreeRight = 3;
			$vhandle->query("UPDATE gdn_category SET TreeLeft = `TreeLeft` + 2 WHERE `TreeLeft` >= 2");
			$vhandle->query("UPDATE gdn_category SET TreeRight = `TreeRight` + 2");
			$insert->execute() or die ($insert->error);
		}
	}

	$insert->close();
	statusDone();

	//exit();

	//Convert subBoards
	statusLog("Converting sub-boards");
	$Query = "SELECT * FROM `smf_1boards` ORDER BY child_level ASC";
	$Result = $handle->query($Query) or die($handle->error.__LINE__);

	//$vhandle->query("DELETE FROM gdn_category WHERE CategoryID != -1");
	$insert = $vhandle->prepare("INSERT INTO gdn_category (`CategoryID`, `ParentCategoryID`, `Depth`, `TreeLeft`, `TreeRight`, `Name`, `Description`, `InsertUserID`, `UpdateUserID`, `DateInserted`, `DateUpdated`, `PermissionCategoryID`, `URLCode`) VALUES (?, ?, ?, ?, ?, ?, ?, 1, 1, FROM_UNIXTIME(1140847200), FROM_UNIXTIME(1140847200), ?, ?)");
	//$inUserID; $inName; $inPassword; $inHashMethod; $inPhoto; $inAbout; $inEmail; $inShowEmail; $inGender; $inCountNotifications;
	$insert->bind_param("iiiiissis", $inCategoryID, $inParentID, $inDepth, $inTreeLeft, $inTreeRight, $inName, $inDescription, $inPermissionCategoryID, $inURLCode); //i=integer

	if($Result->num_rows > 0) {
		while($row = $Result->fetch_assoc()) {
			$row['id_parent'] = $row['id_parent']?$row['id_parent']:$row['id_cat'];

			$inCategoryID = $row['id_board'];
			$inParentID = $row['id_parent'];
			$inName = $row['name'];
			$inDescription = $row['description'];
			$inPermissionCategoryID = -1;
			$inURLCode = niceUrl($row['name']);
			$inDepth = $row['child_level']+2;
			//if($inDepth > 2)
			//	continue;
			$Query = "SELECT * FROM `gdn_category` WHERE CategoryID = ".$row["id_parent"];
			$Result2 = $vhandle->query($Query) or die($handle->error.__LINE__);
			if($Result2->num_rows > 0) {
				$parent = $Result2->fetch_assoc();
				//$inTreeLeft = $parent["TreeLeft"]+1;
				//$inTreeRight = $parent["TreeLeft"]+2;
				$inTreeLeft = $parent["TreeRight"];
				$inTreeRight = $parent["TreeRight"]+1;
				$vhandle->query("UPDATE gdn_category SET TreeLeft = `TreeLeft` + 2, TreeRight = `TreeRight` + 2 WHERE `TreeLeft` > ".$parent["TreeRight"]);
				$vhandle->query("UPDATE gdn_category SET TreeRight = `TreeRight` + 2 WHERE `CategoryID` = ".$parent["CategoryID"]);
				//$vhandle->query("UPDATE gdn_category SET TreeRight = `TreeRight` + 2 WHERE `TreeRight` >= ".$parent["TreeRight"]);
				$insert->execute() or die ($insert->error);
			}
		}
	}

	$insert->close();
	statusDone();
}

//Convert topics
if($convertTopics) {
	statusLog("Converting topics");
	//$Query = "SELECT t.*, m.subject, m.body, m.poster_time, m.poster_ip, m2.poster_time last_poster_time, m2.id_member last_poster FROM smf_1topics t JOIN smf_1messages m ON t.id_first_msg = m.id_msg JOIN smf_1messages m2 ON t.id_last_msg = m2.id_msg LIMIT ".$Start.", ".$Count."";
	$Query = "SELECT t.*, m.subject, m.body, m.poster_time, m.poster_ip, m2.poster_time last_poster_time, m2.id_member last_poster FROM smf_1topics t JOIN smf_1messages m ON t.id_first_msg = m.id_msg JOIN smf_1messages m2 ON t.id_last_msg = m2.id_msg";
	$Result = $handle->query($Query) or die($handle->error.__LINE__);

	$vhandle->query("DELETE FROM gdn_discussion");
	$vhandle->query("START TRANSACTION");
	$insert = $vhandle->prepare("INSERT INTO gdn_discussion (`DiscussionID`, `Type`, `Format`, `ForeignID`, `CategoryID`, `InsertUserID`, `UpdateUserID`, `LastCommentID`, `Name`, `Body`, `DateInserted`, `DateUpdated`, `InsertIPAddress`, `UpdateIPAddress`, `DateLastComment`, `LastCommentUserID`) VALUES (?, ?, 'BBCode', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	//$inUserID; $inName; $inPassword; $inHashMethod; $inPhoto; $inAbout; $inEmail; $inShowEmail; $inGender; $inCountNotifications;
	$insert->bind_param("iiiiiiisssssssi", $inDiscussionID, $inType, $inForeignID, $inCategoryID, $inInsertUserID, $inUpdateUserID, $inLastCommentID, $inName, $inBody, $inDateInserted, $inDateUpdated, $inInsertIPAddress, $inUpdateIPAddress, $inDateLastComment, $inLastCommentUserID); //i=integer

	if($Result->num_rows > 0) {
		while($row = $Result->fetch_assoc()) {
			$inDiscussionID = $row['id_topic'];
			//$inTypeID =;
			//$inForeignID =;
			$inCategoryID = $row['id_board'];
			$inInsertUserID = $row['id_member_started'];
			$inUpdateUserID = $row['id_member_updated'];
			$inLastCommentID = $row['id_last_msg'];
			$inName = strlimit($row['subject'], 100);
			$inBody = $row['body'];
			$inDateInserted = mysqldate($row['poster_time']);
			$inDateUpdated = mysqldate($row['poster_time']);

			$inInsertIPAddress = $row['poster_ip'];
			$inUpdateIPAddress = $row['poster_ip'];
			$inDateLastComment = mysqldate($row['last_poster_time']);
			$inLastCommentUserID = $row['last_poster'];

			$insert->execute() or die ($insert->error);
			//die();
			//print_r($row);
		}
		//echo "<script type=\"text/javascript\">function finLoad() { window.location = \"/convert.php?start=".($Start+$Count)."\"; }</script>";
	}

	$insert->close();
	$vhandle->query("COMMIT");
	statusDone();
}

if($convertPosts) {
	statusLog("Converting posts");
	$Query = "SELECT m.* FROM smf_1messages m JOIN smf_1topics t ON m.id_topic = t.id_topic WHERE m.id_msg != t.id_first_msg LIMIT ".$Start.", ".$Count."";
	$Result = $handle->query($Query) or die($handle->error.__LINE__);

	//$vhandle->query("DELETE FROM gdn_comment");
	$vhandle->query("START TRANSACTION");
	$insert = $vhandle->prepare("INSERT INTO gdn_comment (`CommentID`, `DiscussionID`, `Format`, `Body`, `InsertUserID`, `UpdateUserID`, `DateInserted`, `DateUpdated`, `InsertIPAddress`, `UpdateIPAddress`) VALUES (?, ?, 'BBCode', ?, ?, ?, ?, ?, ?, ?)");
	//$inUserID; $inName; $inPassword; $inHashMethod; $inPhoto; $inAbout; $inEmail; $inShowEmail; $inGender; $inCountNotifications;
	$insert->bind_param("iisiissss", $inCommentID, $inDiscussionID, $inBody, $inInsertUserID, $inUpdateUserID, $inDateInserted, $inDateUpdated, $inInsertIPAddress, $inUpdateIPAddress); //i=integer

	if($Result->num_rows > 0) {
		while($row = $Result->fetch_assoc()) {
			$inCommentID = $row["id_msg"];
			$inDiscussionID = $row["id_topic"];
			$inBody = $row["body"];
			$inInsertUserID = $row["id_member"];
			$inUpdateUserID = $row["id_member"];
			$inDateInserted = mysqldate($row["poster_time"]);
			$inDateUpdated = mysqldate($row["poster_time"]);
			$inInsertIPAddress = $row["poster_ip"];
			$inUpdateIPAddress = $row["poster_ip"];
			$insert->execute() or die ($insert->error);
			//die();
		}
		echo "<script type=\"text/javascript\">function finLoad() { window.location = \"/convert.php?start=".($Start+$Count)."\"; }</script>";
	}

	$insert->close();
	$vhandle->query("COMMIT");
	statusDone();
}

if($convertUsers) {
	//Convert users
	statusLog("Converting users");
	$Query = "SELECT * FROM `smf_1members`";
	$Result = $handle->query($Query) or die($handle->error.__LINE__);

	$vhandle->query("DELETE FROM gdn_user");
	$vhandle->query("START TRANSACTION");
	$insert = $vhandle->prepare("INSERT INTO gdn_user (`UserID`, `Name`, `Password`, `HashMethod`, `Photo`, `About`, `Email`, `ShowEmail`, `Gender`, `CountNotifications`, `DateInserted`, `DateFirstVisit`, `DateLastActive`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	//$inUserID; $inName; $inPassword; $inHashMethod; $inPhoto; $inAbout; $inEmail; $inShowEmail; $inGender; $inCountNotifications;
	$insert->bind_param("issssssisisss", $inUserID, $inName, $inPassword, $inHashMethod, $inPhoto, $inAbout, $inEmail, $inShowEmail, $inGender, $inCountNotifications, $inDateInserted, $inDateFirstVisit, $inDateLastActive); //i=integer

	$genders = array('n', 'm', 'f', 'o');
	if($Result->num_rows > 0) {
		while($row = $Result->fetch_assoc()) {
			$inUserID = $row['id_member'];
			$inName = $row['member_name'];
			$inPassword = $row['passwd'];
			$inHashMethod = "Vanilla";//$row[''];
			$inPhoto = $row['avatar'];
			$inAbout = "";//$row[''];
			$inEmail = $row['email_address'];
			$inShowEmail = $row['hide_email']?0:1;
			$inGender = $genders[$row['gender']];
			$inCountNotifications = 0;//$row[''];
			$inDateInserted = date('Y-m-d H:i:s', $row['date_registered']);
			$inDateFirstVisit = date('Y-m-d H:i:s', $row['date_registered']);
			$inDateLastActive = date('Y-m-d H:i:s', $row['last_login']?$row['last_login']:$row['date_registered']);
			//echo stripslashes($row['member_name']);	
			$insert->execute() or die ($insert->error);
		}
	}
	$insert->close();
	$vhandle->query("COMMIT");
	statusDone();
}

mysqli_close($handle);

?></body></html>