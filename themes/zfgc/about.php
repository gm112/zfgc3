<?php 
	if (!defined('APPLICATION')) exit();

	/**
	 * An associative array of information about this application.
	 */
	$ThemeInfo['mynewtheme'] = array(
	   'Name' => 'ZFGC',
	   'Description' => "The default theme of the ZFGC site.",
	   'Version' => 0.1,
	   'Author' => "MaJoRa",
	   'AuthorEmail' => 'majora31@gmail.com',
	   'AuthorUrl' => 'http://www.zfgc.com/'
	);