<?php if (!defined('APPLICATION')) exit();

$PluginInfo['Affiliates'] = array(
   'Name' => 'Affiliates',
   'Description' => 'Displays Website Affiliates.',
   'Version' => '',
   'Author' => "Phillip Stephens",
   'AuthorEmail' => 'antidote.crk@gmail.com',
   'AuthorUrl' => 'https://wiiking2.com',
   'RegisterPermissions' => array('Plugins.Affiliates.Manage'),
   'SettingsPermission' => array('Plugins.Affiliates.Manage')
);

class AffiliatesPlugin extends Gdn_Plugin {
    
   public function PluginController_Affiliates_Create(&$Sender) {
      $Sender->Permission('Plugins.Affiliates.Manage');
      $Sender->AddSideMenu('plugin/affiliates');
      $Sender->Form = new Gdn_Form();
      $Validation = new Gdn_Validation();
      $ConfigurationModel = new Gdn_ConfigurationModel($Validation);
      $ConfigurationModel->SetField('Affiliates.Location');
      $Sender->Form->SetModel($ConfigurationModel);
            
      if ($Sender->Form->AuthenticatedPostBack() === FALSE) {    
         $Sender->Form->SetData($ConfigurationModel->Data);    
      } else {
         $Data = $Sender->Form->FormValues();
         $ConfigurationModel->Validation->ApplyRule('Affiliates.Location', 'Required');
         if ($Sender->Form->Save() !== FALSE)
            $Sender->StatusMessage = T("Your settings have been saved.");
      }
      
      // creates the page for the plugin options such as display options
      $Sender->Render($this->GetView('settings.php'));
   }

    public function Base_Render_Before(&$Sender) {
        $Controller = $Sender->ControllerName;
        $ShowOnController = array(
                    'discussioncontroller',
                    'categoriescontroller',
                    'discussionscontroller',
                    'profilecontroller',
                    'activitycontroller'
                );
                
       if (!InArrayI($Controller, $ShowOnController)) return;
      // render new block and replace whole thing opposed to just the data
      include_once(PATH_PLUGINS.DS.'Affiliates'.DS.'class.affiliatesmodule.php');
        $AffiliateModule = new AffiliatesModule($Sender);
        $AffiliateModule->GetData();
        $Sender->AddModule($AffiliateModule);
    }
    
    
    public function Base_GetAppSettingsMenuItems_Handler(&$Sender) {
        $Menu = &$Sender->EventArguments['SideMenu'];
        $Menu->AddLink('Add-ons', T('Affiliates'), "plugin/affiliates", "Garden.Themes.Manage");
    }
    
    public function SettingsController_Affiliates_Create($Sender) {
      $Sender->Permission('Garden.Settings.Manage');
      $Sender->Title('Affiliates');
      $Sender->AddSideMenu('plugins/affiliates');
      $Sender->Render($this->GetView('settings.php'));
    }
      
    public function SettingsController_AddAffiliate_Create($Sender) {
        
    }
    
    public function Setup() {
        $this->Structure();
    }
    
    public function Structure() {
        $Structure = Gdn::Structure();
        $Structure->Table('Affiliates')
                ->PrimaryKey('AffiliateId')
                ->Column('Title', 'varchar(256)', FALSE, NULL, 'key')
                ->Column('ImageUrl', 'varchar(512)', FALSE, NULL, 'key')
                ->Column('Url', 'varchar(512)', FALSE, NULL, 'key')
                ->Set();
    }
};