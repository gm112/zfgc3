<?php if (!defined('APPLICATION')) exit();

class AffiliatesModule extends Gdn_Module {
    
    protected $_Affiliates;
    
    public function __construct(&$Sender = '') {
        parent::__construct($Sender);
    }
     
    public function GetData() {
        $Database = Gdn::Database();
        $SQL = $Database->SQL();
        
        $this->_Affiliates = $SQL->Select('a.Title, a.ImageUrl, a.Url')
                                 ->From('Affiliates a')
                                 ->OrderBy('rand()')
                                 ->Get();
    }
    
    public function GetAffiliateCount() {
        $Database = Gdn::Database();
        $SQL = $Database->SQL();
        
        return $SQL->From('Affiliates')->GetCount();
    }
     
    public function AssetTarget() {
        return C('Affiliates.Location', 'Panel');
    }

    public function ToString() {
        $String = '';
        $Session = Gdn::Session();
        $ConfigItem = C('Affiliates.Location', 'Panel');
        ob_start();
        ?>
           <?php
           if ($ConfigItem == 'Panel') {
               echo ('<div id="Affiliates" class="Box">');
               echo ('<h4>'.T("Affiliates") .'</h4>');
               //echo ('<ul>');
           } else {
               echo ('<div id="Affiliates" style="text-align:center">');
           } ?>
            

                    <?php
                        if ($this->_Affiliates->NumRows() > 0) {
                            foreach($this->_Affiliates->Result() as $Affiliate) {
                                if($ConfigItem == 'Panel') {
                        //            echo ('<li>');
                                }             
                   ?>
                          <?php echo ('<a title="'.htmlspecialchars($Affiliate->Title).'" href="'.$Affiliate->Url.'">'
                                .Img($Affiliate->ImageUrl, array('alt' => htmlspecialchars($Affiliate->Title), 'border' => '0'))
                                .'</a>'); ?>
                   <?php
                                if($ConfigItem == 'Panel') {
                      //              echo ('</li>');
                                }   
                            }
                        }
                    //echo ('</ul>');
                    ?>
            </div>
        <?php
        ?>
        
        <?php
        $String = ob_get_contents();
        @ob_end_clean();
        return $String;
    }
};
