<?php if (!defined('APPLICATION')) exit();
echo $this->Form->Open();
echo $this->Form->Errors();
?>

<h1><?php echo T('Affiliates'); ?></h1>
      <div class="Info"><?php echo T('Where should the plugin be shown?'); ?></div>
      <table class="AltRows">
          <thead>
              <tr>
                  <th><?php echo T('Location') ?></th>
                  <th class="Alt"><?php echo T('Description'); ?></th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <th><?php
                    echo $this->Form->Radio('Affiliates.Location', "Side Panel", array('value' => 'Panel', 'selected' => 'selected'));
                  ?></th>
                  <td class="Alt"><?php echo T("This will show the affiliate panel in the Side Panel."); ?></td>
              </tr>
              <tr>
                  <th><?php
                    echo $this->Form->Radio('Affiliates.Location', "Footer", array('value' => 'Foot', 'selected' => 'selected'));
                  ?></th>
                  <td class="Alt"><?php echo T("This will show the affiliate panel on the side of the page."); ?></td>
              </tr>
          </tbody>
      </table>
      <table class="AltRows">
          <thead>
              <tr>
                  <th><?php echo T('Image') ?></th>
                  <th><?php echo T('Title') ?></th>
                  <th><?php echo T('Url')   ?></th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
<?php echo $this->Form->Close('Save');